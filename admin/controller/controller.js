function renderProduct(productArray){
    var contentHTML = "";
      productArray.forEach(function(item,index){
  content=`
      <tr >
      <td><img style="width:110px;height:100px" src="${item.img}" alt="" /></td>
        <td> ${item.name }</td>
        <td> ${item.price}</td>
        <td> ${item.screen}</td>
        <td> ${item.backCamera}</td>
        <td> ${item.frontCamera}</td>
        <td> ${item.desc}</td>
        <td> ${item.type}</td>
       <td> <button onclick="deleteSP('${item.id}')" class="btn btn-danger"><i class="fa fa-trash-alt"></i> DELETE</button>
       <button onclick="showInformation(${item.id})" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="btn btn-warning"><i class="fa fa-wrench"></i> SHOW </button>
       </td>
       </tr>
  `
  contentHTML+=content;
      });
      
      document.getElementById("tbodySinhVien").innerHTML=contentHTML;
  }
  
  function renderAcending(productArray){
    var contentHTML = "";
    productArray.sort(function(a, b) {
      return a.price - b.price; 
    });
    productArray.forEach(function(item,index){
  content=`
    <tr >
    <td><img style="width:110px;height:100px" src="${item.img}" alt="" /></td>
      <td> ${item.name}</td>
      <td> ${item.price}</td>
      <td> ${item.screen}</td>
      <td> ${item.backCamera}</td>
      <td> ${item.frontCamera}</td>
      <td> ${item.desc}</td>
      <td> ${item.type}</td>
     <td> <button onclick="deleteSP('${item.id}')" class="btn btn-danger"><i class="fa fa-trash-alt"></i> DELETE</button>
     <button onclick="showInformation(${item.id})" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="btn btn-warning"><i class="fa fa-wrench"></i> SHOW </button>
     </td>
     </tr>
  `
  contentHTML+=content;
    });
    document.getElementById("tbodySinhVien").innerHTML=contentHTML;
  }
  function renderDecending(productArray){
    var contentHTML = "";
    productArray.sort(function(a, b) {
      return b.price - a.price; 
    });
    productArray.forEach(function(item,index){
  content=`
    <tr >
    <td><img style="width:110px;height:100px" src="${item.img}" alt="" /></td>
      <td> ${item.name}</td>
      <td> ${item.price}</td>
      <td> ${item.screen}</td>
      <td> ${item.backCamera}</td>
      <td> ${item.frontCamera}</td>
      <td> ${item.desc}</td>
      <td> ${item.type}</td>
     <td> <button onclick="deleteSP('${item.id}')" class="btn btn-danger"><i class="fa fa-trash-alt"></i> DELETE</button>
     <button onclick="showInformation(${item.id})" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="btn btn-warning"><i class="fa fa-wrench"></i> SHOW </button>
     </td>
     </tr>
  `
  contentHTML+=content;
    });
    document.getElementById("tbodySinhVien").innerHTML=contentHTML;
  }
  
  function layThongTinTuForm() {
      var img = document.getElementById("product_img").value;
      var name = document.getElementById("product_name").value;
      var price = document.getElementById("product_price").value;
      var screen = document.getElementById("product_screen").value;
      var bcamera = document.getElementById("product_bcamera").value;
      var fcamera = document.getElementById("product_fcamera").value;
      var desc = document.getElementById("product_desc").value;
      var type = document.getElementById("product_type").value;
  
      // không có id trong create
      return {
          img:img,
          name:name,
          price:price,
          screen:screen,
          backCamera:bcamera,
          frontCamera:fcamera,
          desc:desc,
          type:type    
      };
    }
    function tatLoading(){
      document.getElementById("spinner").style.display = "none";
    };
    function batLoading(){
      document.getElementById("spinner").style.display = "flex";
    }
    function showThongTinLenForm(sp){
      document.getElementById("product_img").value=sp.img ;
      document.getElementById("product_name").value=sp.name ;
      document.getElementById("product_price").value=sp.price;
      document.getElementById("product_screen").value=sp.screen;
      document.getElementById("product_bcamera").value=sp.backCamera;
      document.getElementById("product_fcamera").value=sp.frontCamera;
      document.getElementById("product_desc").value=sp.desc;
      document.getElementById("product_type").value=sp.type;
  
    }
  
    function renderResult(productArray){
  let result=  document.getElementById("product_search").value;
      var contentHTML = "";
      productArray.forEach(function(item){
        if(result==item.name){ 
  var content=`
  <div class="product_item my-4 py-4 pl-2" >
  <img style="width:100%; height:300px" src="${item.img}" alt="" />
    <h2> ${item.name}</h2>
    <p><b>Price:</b> <b style="color:red; font-size:30px;width="100%""> ${item.price}$</b></p>
    <p><b>Screen:</b> ${item.screen}</p>
    <p><b>Back Camera:</b> ${item.backCamera}</p>
    <p><b>Front Camera:</b> ${item.frontCamera}</p>
    <p><b>Describe:</b> ${item.desc}</p>
    <p><b>Type:</b> ${item.type}</p>
    <td> <button onclick="deleteSP('${item.id}')" class="btn btn-danger"><i class="fa fa-trash-alt"></i> DELETE</button>
    <button onclick="showInformation(${item.id})" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="btn btn-warning"><i class="fa fa-wrench"></i> SHOW </button>
    </td>
  </div>  
  `
  contentHTML+=content;}
  
      });
      document.getElementById("search_result").innerHTML=contentHTML;
  }
  