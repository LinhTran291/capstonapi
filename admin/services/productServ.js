const BASE_URL ="https://64802da5f061e6ec4d48bccc.mockapi.io/product";
var productServ = {
  //lấy mảng dữ liệu từ dưới MockAPI 
  getList:()=>{
    return axios({
      url: BASE_URL,
      method: 'GET',
    })
  },
  create: (product) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
},
   delete: (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  }) 
  },
  update:(id,product)=>{
    return axios({
      url: `${BASE_URL}/${id}`,
    method: "PUT",
    data: product,
  })
},
getID:(id)=>{
  return axios({
    url: `${BASE_URL}/${id}`  ,
    method: "GET",
  });
},

}
