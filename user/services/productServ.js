const BASE_URL ="https://64802da5f061e6ec4d48bccc.mockapi.io/product";
var productServ = {
  //lấy mảng dữ liệu từ dưới MockAPI 
  getList: () => {
    return axios({
       url: BASE_URL,
      method: "GET",
    });
  },
  create: (product) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
},
getID:(id)=>{
  return axios({
    url: `${BASE_URL}/${id}`  ,
    method: "GET",
  });
},

}
const BASE="https://64802da5f061e6ec4d48bccc.mockapi.io/cart_item";
var productCart={
  getList: () => {
    return axios({
       url: BASE,
      method: "GET",
    });
  },
  create: (product) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: product,
    });
},

}